import React from "react"
import styled from "styled-components"
import { Link } from "gatsby"

import { device } from "./theme/device"

const StyledLink = styled(Link)`
  display: block;
  text-decoration: none;
  text-align: center;
  margin: 0 auto;
  width: 80%;

  @media ${device.tablet} {
    width: 50%;
  }

  @media ${device.mobile} {
    width: 75%;
  }
`

const LightLink = styled(StyledLink)`
  border: ${props => `2px solid ${props.theme.colors.white}`};
  padding: 1rem 3rem;
  font-size: ${props => props.theme.fontSizes[3]};
  color: ${props => props.theme.colors.white};
  border-radius: 10px;
  overflow: hidden;

  vertical-align: middle;
  transform: perspective(1px) translateZ(0);
  box-shadow: 0 0 1px rgba(0, 0, 0, 0);
  position: relative;
  transition-property: color;
  transition-duration: 0.3s;

  &::before {
    content: "";
    position: absolute;
    z-index: -1;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: ${props => props.theme.colors.white};
    transform: scaleX(0);
    transform-origin: 0 50%;
    transition-property: transform;
    transition-duration: 0.3s;
    transition-timing-function: ease-out;
  }

  &:hover,
  &:focus,
  &:active {
    color: ${props => props.theme.colors.primary.dark};
  }

  &:hover::before,
  &:focus::before,
  &:active::before {
    transform: scaleX(1);
  }
`

export const CTALight = ({ text, link }) => (
  <LightLink to={link}>{text}</LightLink>
)
