import React from "react"
import styled from "styled-components"

const Container = styled.footer`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 1rem;
  text-align: center;

  position: absolute;
  bottom: 0;
  left: 50%;
  z-index: 100;
  transform: translateX(-50%);
`

const Text = styled.small`
  color: ${props => props.theme.colors.white};
  font-size: ${props => props.theme.fontSizes[1]};
`

const Footer = props => (
  <Container>
    <Text>&copy; {new Date().getFullYear()} | KindCard</Text>
  </Container>
)

export default Footer
