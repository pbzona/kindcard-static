import React from "react"
import styled from "styled-components"
import PropTypes from "prop-types"

const FieldWrapper = styled.div`
  display: block;
  margin-bottom: 2rem;
`

const Label = styled.label`
  display: block;
  color: ${props => props.theme.colors.primary.base};
  font-size: ${props => props.theme.fontSizes[3]};
  line-height: ${props => props.theme.lineHeights[3]};
  font-family: ${props => props.theme.fonts.heading};
  font-weight: 600;
  text-transform: uppercase;
  letter-spacing: 1px;
`

const Input = styled.input`
  display: block;
  width: 100%;
  margin: ${props => `${props.theme.sizes[1]} 0 ${props.theme.sizes[3]}`};
  color: ${props => props.theme.colors.gray};
  font-size: ${props => props.theme.fontSizes[3]};
  line-height: ${props => props.theme.lineHeights[3]};
  padding: 1.2rem 3rem;
  border: 0;
  font-family: ${props => props.theme.fonts.body};

  &:invalid {
    box-shadow: none;
    outline: ${props => `3px solid ${props.theme.colors.support2.light}`};
  }

  &:active,
  &:focus {
    outline: ${props => `3px solid ${props.theme.colors.primary.base}`};
  }
`

const TextArea = styled.textarea`
  display: block;
  width: 100%;
  margin: ${props => `${props.theme.sizes[1]} 0 ${props.theme.sizes[3]}`};
  color: ${props => props.theme.colors.gray};
  font-size: ${props => props.theme.fontSizes[3]};
  line-height: ${props => props.theme.lineHeights[3]};
  font-family: ${props => props.theme.fonts.body};
  padding: 1.6rem 3rem;
  border: 0;
  height: 16rem;
  resize: none;

  &:invalid {
    box-shadow: none;
    outline: ${props => `3px solid ${props.theme.colors.support2.light}`};
  }

  &:active,
  &:focus {
    outline: ${props => `3px solid ${props.theme.colors.primary.base}`};
  }
`

const Submit = styled.button`
  background-color: ${props => props.theme.colors.primary.base};
  border: none;
  border-radius: 0px;
  color: ${props => props.theme.colors.white};
  padding: 1rem 3rem;
  width: 100%;
  text-transform: uppercase;
  font-size: ${props => props.theme.fontSizes[4]};
  line-height: ${props => props.theme.lineHeights[4]};
  letter-spacing: 1px;
  font-family: ${props => props.theme.fonts.heading};
  font-weight: 600;
  transition: 0.15s all ease-in;

  &:hover,
  &:focus,
  &:active {
    cursor: pointer;
    background: ${props => props.theme.colors.primary.dark};
  }

  &:focus,
  &:active {
    outline: none;
  }
`

// Standard input field
export const InputField = ({
  label,
  type,
  name,
  value,
  handleChange,
  handleFocus,
  pattern,
}) => (
  <FieldWrapper>
    <Label>
      {label}
      <Input
        type={type}
        name={name}
        value={value}
        onChange={handleChange}
        onFocus={handleFocus}
        required
        pattern={pattern}
      />
    </Label>
  </FieldWrapper>
)

InputField.propTypes = {
  label: PropTypes.string,
  type: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.any,
  handleChange: PropTypes.func,
  handleFocus: PropTypes.func,
}

InputField.defaultProps = {
  label: `Input`,
  type: `text`,
  name: `textinput`,
  value: ``,
  handleChange: {},
  handleFocus: {},
}

// Input field with custom pattern
export const ValidateInputField = ({
  label,
  type,
  name,
  value,
  handleChange,
  handleFocus,
  pattern,
}) => (
  <FieldWrapper>
    <Label>
      {label}
      <Input
        type={type}
        name={name}
        value={value}
        onChange={handleChange}
        onFocus={handleFocus}
        required
        pattern={pattern}
      />
    </Label>
  </FieldWrapper>
)

ValidateInputField.propTypes = {
  label: PropTypes.string,
  type: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.any,
  handleChange: PropTypes.func,
  handleFocus: PropTypes.func,
  pattern: PropTypes.string,
}

ValidateInputField.defaultProps = {
  label: `Input`,
  type: `text`,
  name: `textinput`,
  value: ``,
  handleChange: {},
  handleFocus: {},
  pattern: `*`,
}

// Text area
export const TextField = ({
  label,
  name,
  value,
  handleChange,
  handleFocus,
}) => (
  <FieldWrapper>
    <Label>
      {label}
      <TextArea
        name={name}
        value={value}
        onChange={handleChange}
        onFocus={handleFocus}
        required
      />
    </Label>
  </FieldWrapper>
)

TextField.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.any,
  handleChange: PropTypes.func,
  handleFocus: PropTypes.func,
}

TextField.defaultProps = {
  label: `Your message`,
  name: `textarea`,
  value: ``,
  handleChange: {},
  handleFocus: {},
}

// Button to be used for submitting forms
export const SubmitButton = ({ text }) => <Submit type="submit">{text}</Submit>

Submit.propTypes = {
  text: PropTypes.string,
}

Submit.defaultProps = {
  text: `Submit`,
}
