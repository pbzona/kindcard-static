import React from "react"
import styled from "styled-components"

import {
  InputField,
  ValidateInputField,
  TextField,
  SubmitButton,
} from "./Inputs"

const Form = styled.form``

const Fieldset = styled.fieldset`
  border: none;

  :disabled {
    opacity: 0.8;
    cursor: not-allowed;
  }
`

// Full share story form
const ShareYourStoryForm = ({
  values,
  handleInputChange,
  handleInputFocus,
  handleSubmit,
}) => (
  <Form onSubmit={handleSubmit}>
    <Fieldset disabled={values.submitting}>
      <ValidateInputField
        label="Card number"
        name="cardNumber"
        type="text"
        value={values.cardNumber}
        handleChange={handleInputChange}
        handleFocus={handleInputFocus}
        pattern="\d{3}\d?"
      />
      <InputField
        label="Email"
        name="email"
        type="email"
        value={values.email}
        handleChange={handleInputChange}
        handleFocus={handleInputFocus}
      />
      <InputField
        label="Where did you receive your card?"
        name="location"
        type="text"
        value={values.location}
        handleChange={handleInputChange}
        handleFocus={handleInputFocus}
      />
      <TextField
        label="How did you receive your card?"
        name="story"
        value={values.story}
        handleChange={handleInputChange}
        handleFocus={handleInputFocus}
      />
      <SubmitButton text="Submit" />
    </Fieldset>
  </Form>
)

export default ShareYourStoryForm
