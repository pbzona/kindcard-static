import React from "react"
import styled from "styled-components"
import Youtube from "../svg/youtube.svg"
import Instagram from "../svg/instagram.svg"

const SocialContainer = styled.div`
  display: flex;
  justify-content: space-around;
  width: 50%;
  margin: 0 auto;
`

const Platform = styled.div`
  color: ${props => props.theme.colors.white};
  font-size: ${props => props.theme.fontSizes[3]};
  text-transform: uppercase;
`

const SocialIcon = styled.img`
  height: 3.2rem;
  width: 3.2rem;
`

const SocialSmall = () => (
  <SocialContainer>
    <Platform>
      <a href="https://youtu.be/DKiHBh1TQ6E">
        <SocialIcon src={Youtube} />
      </a>
    </Platform>
    <Platform>
      <a href="https://www.instagram.com/kindcard_ig/">
        <SocialIcon src={Instagram} />
      </a>
    </Platform>
  </SocialContainer>
)

export default SocialSmall
