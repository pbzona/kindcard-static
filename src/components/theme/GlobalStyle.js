import { createGlobalStyle } from "styled-components"
import { device } from "./device"

const GlobalStyle = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Roboto:700,900|Source+Sans+Pro:400,600,700');

  *, *::before, *::after {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
  }

  html {
    font-size: 62.5%;

    @media ${device.tabletLg} {
      font-size: 56.25%;
    }
    @media ${device.tablet} {
      font-size: 50%;
    }
    @media ${device.mobile} {
      font-size: 43.75%;
    }
    @media ${device.desktop} {
      font-size: 75%;
    }
  }

  body {
    font-family: 'Source Sans Pro', Arial, Helvetica, sans-serif;
  }

  h1,h2,h3,h4,h5 {
    font-family: 'Roboto', Arial, Helvetica, sans-serif;
  }
`

export default GlobalStyle
