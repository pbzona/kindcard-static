// This file gets exported directly for use in component styling, NOT through theme index

const size = {
  mobileSm: "500px",
  mobile: "600px",
  tablet: "900px",
  tabletLg: "1200px",
  desktop: "1800px",
}

export const device = {
  mobileSm: `only screen and (max-width: ${size.mobileSm})`,
  mobile: `only screen and (max-width: ${size.mobile})`,
  tablet: `only screen and (max-width: ${size.tablet})`,
  tabletLg: `only screen and (max-width: ${size.tabletLg})`,
  desktop: `only screen and (min-width: ${size.desktop})`,
}
