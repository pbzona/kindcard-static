const fontSizes = [
  "1.2rem",
  "1.4rem",
  "1.6rem",
  "1.8rem",
  "2rem",
  "2.4rem",
  "3rem",
  "3.6rem",
  "4.8rem",
  "6rem",
  "7.2rem",
  "9.6rem",
]

export default fontSizes
