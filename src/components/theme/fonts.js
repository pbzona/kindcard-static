const fonts = {
  heading: `'Roboto', Helvetica, Arial, sans-serif`,
  body: `'Source Sans Pro', Helvetica, Arial, sans-serif`,
}

export default fonts
