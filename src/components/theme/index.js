import colors from "./colors"
import fonts from "./fonts"
import fontSizes from "./fontSizes"
import lineHeights from "./lineHeights"
import sizes from "./sizes"

export default {
  colors,
  fonts,
  fontSizes,
  lineHeights,
  sizes,
}
