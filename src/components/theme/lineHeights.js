const lineHeights = [
  "1.5rem",
  "1.8rem",
  "2rem",
  "2.2rem",
  "2.4rem",
  "2.8rem",
  "3.6rem",
  "4.2rem",
  "5.6rem",
  "7.2rem",
  "8.4rem",
  "11rem",
]

export default lineHeights
