const sizes = [
  ".4rem",
  ".8rem",
  "1.2rem",
  "1.6rem",
  "2.4rem",
  "3.2rem",
  "4.8rem",
  "6.4rem",
  "9.6rem",
  "12.8rem",
  "19.2rem",
  "25.6rem",
  "38.4rem",
  "51.2rem",
  "64rem",
  "76.8rem",
]

export default sizes
