import React from "react"
import styled from "styled-components"
import { Link } from "gatsby"
import PropTypes from "prop-types"

import SEO from "../components/seo"
import Global from "./Global"
import Footer from "../components/Footer"

import { device } from "../components/theme/device"

// This is the template for pages that don't have an intended next step in the user flow.
// Ex. thank you for sending an email, 404, error

const PageContainer = styled.div`
  width: 100vw;
  height: 100vh;
  position: relative;
  padding: 4rem 0;
  background-image: ${props =>
    `linear-gradient(to bottom right, ${props.theme.colors.primary.base},${
      props.theme.colors.primary.dark
    })`};
`

const Content = styled.main`
  background-color: ${props => props.theme.colors.white};
  width: 60%;
  margin: auto;
  max-width: 90rem;
  box-shadow: 6px 6px 20px rgba(56, 56, 56, 0.2);
  border-radius: 20px;
  padding: 4rem 6rem;

  color: ${props => props.theme.colors.primary.base};

  @media ${device.tablet} {
    width: 85%;
  }
`

const Title = styled.h1`
  font-size: ${props => props.theme.fontSizes[7]};
  line-height: ${props => props.theme.lineHeights[7]};
  letter-spacing: 2px;
  margin-bottom: 2rem;
`

const Description = styled.p`
  font-size: ${props => props.theme.fontSizes[3]};
  line-height: ${props => props.theme.lineHeights[3]};
  color: ${props => props.theme.colors.primary.dark};
  margin-bottom: 4rem;
`

const LinkBack = styled(Link)`
  font-size: ${props => props.theme.fontSizes[2]};
  line-height: ${props => props.theme.lineHeights[2]};
  color: ${props => props.theme.colors.white};
  background-color: ${props => props.theme.colors.primary.base};
  padding: 0.6rem 3rem;
  text-decoration: none;
  box-shadow: 3px 3px 8px rgba(56, 56, 56, 0.2);
  transition: 0.15s all ease-in;

  &:hover,
  &:focus,
  &:active {
    background-color: ${props => props.theme.colors.primary.dark};
  }
`

const EndPage = ({ title, description, linkTo, linkText, seo }) => (
  <Global>
    <SEO title={seo} />
    <PageContainer>
      <Content>
        <Title>{title}</Title>
        <Description>{description}</Description>
        <LinkBack to={linkTo}>{linkText}</LinkBack>
      </Content>
      <Footer />
    </PageContainer>
  </Global>
)

EndPage.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  linkTo: PropTypes.string.isRequired,
  linkText: PropTypes.string.isRequired,
  seo: PropTypes.string.isRequired,
}

export default EndPage
