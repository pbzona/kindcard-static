import React from "react"
import { ThemeProvider } from "styled-components"
import Theme from "../components/theme"
import GlobalStyle from "../components/theme/GlobalStyle"

const Global = ({ children }) => (
  <ThemeProvider theme={Theme}>
    <>
      <GlobalStyle />
      {children}
    </>
  </ThemeProvider>
)

export default Global
