import React from "react"

import EndPage from "../layouts/EndPage"

const title = "404 - Not found"
const description = `
Sorry! We couldn't find this page.
`
const linkTo = "/"
const linkText = "Go Back Home"
const seo = "404"

const NotFoundPage = () => (
  <EndPage
    title={title}
    description={description}
    linkTo={linkTo}
    linkText={linkText}
    seo={seo}
  />
)

export default NotFoundPage
