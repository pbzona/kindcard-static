import React from "react"

import EndPage from "../layouts/EndPage"

const title = "Oops!"
const description = `
Something went wrong with your request - please try again in a few minutes.
`
const linkTo = "/"
const linkText = "Go Back Home"
const seo = "Error"

const ErrorPage = () => (
  <EndPage
    title={title}
    description={description}
    linkTo={linkTo}
    linkText={linkText}
    seo={seo}
  />
)

export default ErrorPage
