import React from "react"
import styled from "styled-components"
import Img from "gatsby-image"
import { graphql, StaticQuery } from "gatsby"

import Global from "../layouts/Global"
import SEO from "../components/seo"
import { CTALight } from "../components/CTA"
import Footer from "../components/Footer"

import { device } from "../components/theme/device"
import SocialSmall from "../components/SocialSmall"

// Break this into a config file eventually
const DescriptionText = `
How do random acts of kindness map over time? This is the question we created KindCard to answer. People do nice things for each other every day - paying for the person's coffee behind them in line, or stopping to help someone carry their groceries. It's easy to go about our days and never think twice about these events. KindCard aims to track these interactions to show the true impact of a random act of kindness.
`

// Main page structure
const PageContainer = styled.main`
  width: 100vw;
  height: 100vh;

  position: relative;
`

const BackgroundImage = styled(Img)`
  position: absolute;
  top: 0;
  left: -10%;
  width: 100%;
  height: 100%;
  z-index: 10;

  @media ${device.tablet} {
    top: -25%;
    left: 0;
  }
`

const LayerContainer = styled.div`
  width: 100vw;
  height: 100vh;

  display: flex;
  flex-direction: row;
  position: absolute;
  top: 0;
  left: 0;

  @media ${device.tablet} {
    height: 100%;
    flex-direction: column;
  }
`

const LayerLeft = styled.div`
  position: relative;
  flex-grow: 1;
  height: 100%;

  @media ${device.tablet} {
    height: 100%;
    flex-grow: 0;
  }
`

const LayerRight = styled.div`
  position: relative;
  width: 60rem;
  max-width: 40%;
  flex-grow: 1;
  padding: 0;
  min-height: 0;

  @media ${device.tablet} {
    width: 100%;
    max-width: 100%;
    flex-grow: 2;
    min-height: 65%;

    &::before {
      content: "";
      position: absolute;
      top: 0;
      left: 0;
      height: 2px;
      width: 100vw;
      background: ${props => props.theme.colors.primary.light};
      z-index: 100;
    }
  }
`

// Left side
const BackgroundOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;

  background-image: ${props =>
    `linear-gradient(to bottom right, ${props.theme.colors.primary.base},${
      props.theme.colors.primary.dark
    })`};
  opacity: 0.82;
  z-index: 20;
`

const TitleContainer = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 30;
  display: flex;
  flex-direction: column;
`

const Title = styled.h1`
  font-size: ${props => props.theme.fontSizes[11]};
  font-weight: 700;
  color: ${props => props.theme.colors.white};
  letter-spacing: 5px;

  @media ${device.mobile} {
    font-size: ${props => props.theme.fontSizes[10]};
  }
`

const Subtitle = styled.h2`
  font-size: ${props => props.theme.fontSizes[6]};
  color: ${props => props.theme.colors.white};
  letter-spacing: 2px;
`

const Hr = styled.hr`
  width: 80%;
  border: 0;
  height: 2px;
  background-color: ${props => props.theme.colors.white};
  margin: 1.4rem auto;
  opacity: 0.6;
`

// Right side
const BackgroundSolid = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  width: 100%;
  height: 100%;
  background-image: ${props =>
    `linear-gradient(to bottom right, ${props.theme.colors.primary.base},${
      props.theme.colors.primary.dark
    })`};
  z-index: 20;
  box-shadow: -3px 0 20px rgba(56, 56, 56, 0.2);

  @media ${device.tablet} {
    box-shadow: none;
  }
`

const TextContainer = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 30;
  width: 80%;
  margin: 0 auto;
  color: ${props => props.theme.colors.white};
  padding: 3rem;

  @media ${device.tablet} {
    margin: 0 0 10rem;
    width: 90%;
    top: 45%;
  }

  @media ${device.mobile} {
    width: 95%;
  }
`

const DescriptionHeader = styled.h3`
  font-size: ${props => props.theme.fontSizes[7]};
  text-align: center;
  margin-bottom: 2rem;
  text-transform: uppercase;
  letter-spacing: 7px;

  @media ${device.tablet} {
    margin-bottom: 1.2rem;
  }
`

const DescriptionContent = styled.p`
  text-align: justify;
  font-size: ${props => props.theme.fontSizes[3]};
  line-height: ${props => props.theme.lineHeights[5]};
  margin-bottom: 3rem;

  @media ${device.tablet} {
    font-size: ${props => props.theme.fontSizes[4]};
    line-height: ${props => props.theme.lineHeights[5]};
  }
`

const PageContent = ({ data }) => (
  <Global>
    <SEO
      title="Home"
      keywords={[`KindCard`, `Kind Card`, `KindCardMap`]}
      home={true}
    />
    <PageContainer>
      <BackgroundImage fluid={data.placeholderImage.childImageSharp.fluid} />
      <LayerContainer>
        <LayerLeft>
          <BackgroundOverlay />
          <TitleContainer>
            <Title>{data.site.siteMetadata.title}</Title>
            <Subtitle>{data.site.siteMetadata.description}</Subtitle>
            <Hr />
            <SocialSmall />
          </TitleContainer>
        </LayerLeft>
        <LayerRight>
          <BackgroundSolid />
          <TextContainer>
            <DescriptionHeader>About Us</DescriptionHeader>
            <DescriptionContent>{DescriptionText}</DescriptionContent>
            <CTALight text="Enter your card &rarr;" link="/share-your-story" />
          </TextContainer>
        </LayerRight>
      </LayerContainer>
      <Footer />
    </PageContainer>
  </Global>
)

const IndexPage = () => (
  <StaticQuery
    query={graphql`
      query {
        site {
          siteMetadata {
            title
            description
          }
        }
        placeholderImage: file(relativePath: { eq: "map-bg.jpg" }) {
          childImageSharp {
            fluid(maxWidth: 2000) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    `}
    render={data => <PageContent data={data} />}
  />
)

export default IndexPage
