import React from "react"
import styled from "styled-components"
import axios from "axios"
import ls from "local-storage"
import { navigate } from "gatsby"

import Global from "../layouts/Global"
import SEO from "../components/seo"
import ShareYourStoryForm from "../components/ShareYourStoryForm"
import Footer from "../components/Footer"

import { device } from "../components/theme/device"

const PageContainer = styled.main`
  min-height: 100vh;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  background-image: ${props =>
    `linear-gradient(to bottom right, ${props.theme.colors.primary.base},${
      props.theme.colors.primary.dark
    })`};
  padding: 4rem 0;

  @media ${device.mobile} {
    align-items: start;
  }
`

const FormContainer = styled.div`
  background-color: ${props => props.theme.colors.white};
  width: 65rem;
  box-shadow: 6px 6px 20px rgba(56, 56, 56, 0.2);
  border-radius: 20px;
  padding: 4rem 6rem;

  @media ${device.mobile} {
    width: 90%;
    padding: 2.8rem 3.2rem;
  }
`

const FormHeader = styled.h1`
  color: ${props => props.theme.colors.primary.dark};
  font-size: ${props => props.theme.fontSizes[7]};
  text-align: center;
  margin-bottom: 2rem;
  text-transform: uppercase;
  letter-spacing: 3px;
`
class ShareYourStory extends React.Component {
  constructor(props) {
    super(props)

    // Use try/catch to prevent `window is undefined` error during build
    try {
      ls.backend(window.sessionStorage)
    } catch (e) {
      console.log(e)
    }
  }

  data = JSON.parse(ls.get("kindcard-data"))

  init = {
    // Must check whether data is defined otherwise it errors out when assigning
    cardNumber: this.data ? this.data.cardNumber : "000",
    email: this.data ? this.data.email : "you@example.com",
    location: this.data ? this.data.location : "Enter a city/zip code",
    story: this.data ? this.data.story : "Share your story",
    submitting: false,
  }

  state = {
    ...this.init,
  }

  handleInputChange = e => {
    const value = e.target.value
    const name = e.target.name

    this.setState({
      [name]: value,
    })
  }

  handleInputFocus = e => {
    const value = e.target.value
    const name = e.target.name

    if (this.state[name] === this.init[name]) {
      this.setState({
        [name]: "",
      })
    }
  }

  handleSubmit = event => {
    event.preventDefault()
    this.setState({
      submitting: true,
    })

    ls.set("kindcard-data", JSON.stringify(this.state))

    axios({
      method: "POST",
      // TODO - Abstract this out
      url:
        "https://ihiz3bgs1h.execute-api.us-east-1.amazonaws.com/prod/KindCard_ShareStory",
      data: JSON.stringify(this.state),
      mode: "no-cors",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then(res => {
        navigate("/thank-you-for-sharing/")
      })
      .catch(err => {
        console.error(err)
        navigate("/error/")
      })
  }

  render() {
    return (
      <Global>
        <SEO
          title="Share Your Story"
          keywords={[`KindCard`, `Kind Card`, `KindCardMap`]}
        />
        <PageContainer>
          {console.log(ls.backend())}
          <FormContainer>
            <FormHeader>Share Your Story</FormHeader>
            <ShareYourStoryForm
              values={this.state}
              handleInputChange={this.handleInputChange}
              handleInputFocus={this.handleInputFocus}
              handleSubmit={this.handleSubmit}
            />
          </FormContainer>
          <Footer />
        </PageContainer>
      </Global>
    )
  }
}

export default ShareYourStory
