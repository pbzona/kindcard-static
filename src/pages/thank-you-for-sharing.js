import React from "react"

import EndPage from "../layouts/EndPage"

const title = "Thank you for sharing your story!"
const description = `
The full website is in the process of being built, and will include features such as being able to see a map of where each card has traveled, stories of how the cards were passed along, and much more. Until then, we’ll be sure to email you any updates involving your card!
`
const linkTo = "/"
const linkText = "Go Back Home"
const seo = "Thank you"

const ThankYouForSharingPage = () => (
  <EndPage
    title={title}
    description={description}
    linkTo={linkTo}
    linkText={linkText}
    seo={seo}
  />
)

export default ThankYouForSharingPage
